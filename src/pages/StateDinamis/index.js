import React, {Component, useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

const Counter = () => {
  const [nilai, setNilai] = useState(0);
  return (
    <View>
      <Text>{nilai}</Text>
      <Button title="Tambah" onPress={() => setNilai(nilai + 1)} />
      <Button title="Reset" onPress={() => setNilai(0)} color="red" />
    </View>
  );
};

class CounterClass extends Component {
  state = {
    nilai: 0,
  };
  render() {
    return (
      <View>
        <Text>{this.state.nilai}</Text>
        <Button
          title="Tambah"
          onPress={() => this.setState({nilai: this.state.nilai + 1})}
        />
        <Button
          title="Reset"
          onPress={() => this.setState({nilai: 0})}
          color="red"
        />
      </View>
    );
  }
}

const StateDinamis = () => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.textTitle}>Ini materi State Dinamis</Text>
      <Text style={styles.titleSection}>Functional Component</Text>
      <Counter />
      <Text style={styles.titleSection}>Class Component</Text>
      <CounterClass />
    </View>
  );
};

export default StateDinamis;

const styles = StyleSheet.create({
  wrapper: {
    padding: 20,
  },
  textTitle: {
    textAlign: 'center',
  },
  titleSection: {
    marginBottom: 10,
    marginTop: 10,
  },
});
