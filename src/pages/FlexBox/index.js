/* eslint-disable react-native/no-inline-styles */
import React, {Component, useEffect, useState} from 'react';
import {Image, Text, View} from 'react-native';
// class FlexBox extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       subscriber: 200,
//     };
//     console.log('==> constructor');
//   }
//   componentDidMount() {
//     console.log('==> componentDidMount');
//     setTimeout(() => {
//       this.setState({subscriber: 400});
//     }, 2000);
//   }
//   componentDidUpdate() {
//     console.log('==> componentDidUpdate');
//   }
//   componentWillUnmount() {
//     console.log('==> will unmount');
//   }
//   render() {
//     console.log('==> render');
//     return (
//       <>
//         <View
//           style={{
//             flexDirection: 'row',
//             alignItems: 'center',
//             justifyContent: 'space-between',
//             backgroundColor: '#f5f5f5',
//           }}>
//           <View style={{backgroundColor: '#123409', height: 50, width: 50}} />
//           <View style={{backgroundColor: '#321890', height: 50, width: 50}} />
//           <View style={{backgroundColor: '#456789', height: 50, width: 50}} />
//           <View style={{backgroundColor: '#324571', height: 50, width: 50}} />
//         </View>
//         <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
//           <Text>Home</Text>
//           <Text>About</Text>
//           <Text>Portofolio</Text>
//           <Text>Privacy</Text>
//           <Text>Website</Text>
//         </View>
//         <View style={{flexDirection: 'row', marginTop: 10}}>
//           <Image
//             source={{uri: 'https://source.unsplash.com/igfxlMW0l6Q'}}
//             style={{width: 100, height: 100, borderRadius: 50, marginRight: 14}}
//           />
//           <View>
//             <Text style={{fontSize: 20, fontWeight: 'bold'}}>
//               Mashasi Irika
//             </Text>
//             <Text>{this.state.subscriber} Subscriber</Text>
//           </View>
//         </View>
//       </>
//     );
//   }
// }

const FlexBox = () => {
  const [subscriber, setSubscriber] = useState(200);
  useEffect(() => {
    console.log('did mount');
    setTimeout(() => {
      setSubscriber(600);
    }, 5000);
    return () => {
      console.log('did update');
    };
  }, [subscriber]);
  return (
    <>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: '#f5f5f5',
        }}>
        <View style={{backgroundColor: '#123409', height: 50, width: 50}} />
        <View style={{backgroundColor: '#321890', height: 50, width: 50}} />
        <View style={{backgroundColor: '#456789', height: 50, width: 50}} />
        <View style={{backgroundColor: '#324571', height: 50, width: 50}} />
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <Text>Home</Text>
        <Text>About</Text>
        <Text>Portofolio</Text>
        <Text>Privacy</Text>
        <Text>Website</Text>
      </View>
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <Image
          source={{uri: 'https://source.unsplash.com/igfxlMW0l6Q'}}
          style={{width: 100, height: 100, borderRadius: 50, marginRight: 14}}
        />
        <View>
          <Text style={{fontSize: 20, fontWeight: 'bold'}}>Mashasi Irika</Text>
          <Text>{subscriber} Subscriber</Text>
        </View>
      </View>
    </>
  );
};

export default FlexBox;
