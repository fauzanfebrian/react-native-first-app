import React from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';
import Product from '../../components/Product';

const StylingComponent = () => {
  return (
    <View>
      <Text style={styles.text}>Hello World</Text>
      <View
        style={{
          width: 100,
          height: 100,
          backgroundColor: '#120938',
          borderWidth: 2,
          borderColor: '#109823',
          margin: 20,
        }}
      />

      {/* component macbook */}
      <Product />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    color: '#10ac83',
    fontSize: 24,
    fontWeight: 'bold',
    margin: 20,
  },
});
export default StylingComponent;
