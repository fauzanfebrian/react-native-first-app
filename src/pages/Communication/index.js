import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Cart from '../../components/Cart';
import Product from '../../components/Product';

const Communication = () => {
  const [quantity, setQuantity] = useState(0);
  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>
        Ini Materi Communication Antar Project
      </Text>
      <Cart quantity={quantity} reset={() => setQuantity(0)} />
      <Product onButtonPress={() => setQuantity(quantity + 1)} />
    </View>
  );
};

export default Communication;

const styles = StyleSheet.create({
  container: {padding: 20},
  textTitle: {textAlign: 'center', marginBottom: 10},
});
