import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import WebSvg from '../../assets/images/webdevelop.svg';

const ReactNativeSvg = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>Ini Materi React native svg</Text>
      <WebSvg width={230} height={280} />
    </View>
  );
};

export default ReactNativeSvg;

const styles = StyleSheet.create({
  container: {padding: 20},
  textTitle: {alignItems: 'center'},
});
