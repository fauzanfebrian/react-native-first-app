import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';

const Story = props => {
  return (
    <View style={{alignItems: 'center', marginRight: 20}}>
      <Image
        source={{uri: props.image}}
        style={{width: 70, height: 70, borderRadius: 70}}
      />
      <Text style={{maxWidth: 50, textAlign: 'center'}}>{props.title}</Text>
    </View>
  );
};

const PropsDinamis = () => {
  return (
    <>
      <Text style={{color: '#000312', marginTop: 10}}>
        Props Dinamis Materi
      </Text>
      <ScrollView horizontal>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
          }}>
          <Story title="Code" image="https://source.unsplash.com/hMv_eRKuaL4" />
          <Story
            title="24 / 7"
            image="https://source.unsplash.com/qoeVGIpkx8g"
          />
          <Story
            title="About Me"
            image="https://source.unsplash.com/WzgcVhC4CH4"
          />
          <Story
            title="Headhace"
            image="https://source.unsplash.com/6dEyX6VzrnM"
          />
          <Story
            title="Activity"
            image="https://source.unsplash.com/Wx8pnzc9bRk"
          />
          <Story
            title="Photo"
            image="https://source.unsplash.com/romMz3ed9Qo"
          />
          <Story
            title="Cofee"
            image="https://source.unsplash.com/GKF3RQ-4Tq4"
          />
        </View>
      </ScrollView>
    </>
  );
};

export default PropsDinamis;

const styles = StyleSheet.create({});
