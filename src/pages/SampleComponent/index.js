import React, {Component} from 'react';
import {Image, Text, TextInput, View} from 'react-native';

const SampleComponent = () => {
  return (
    <View style={{backgroundColor: '#f4f9f9'}}>
      <Text>Fauzan Febriansyah</Text>
      <Umur />
      <Photo />
      <TextInput style={{borderWidth: 1}} />
      <Profile />
    </View>
  );
};

const Umur = () => {
  return <Text>16 Tahun</Text>;
};

const Photo = () => {
  return (
    <Image
      source={{uri: 'http://placeimg.com/100/100/tech'}}
      style={{width: 100, height: 100}}
    />
  );
};
class Profile extends Component {
  render() {
    return (
      <View>
        <Image
          source={{uri: 'http://placeimg.com/250/250/animals'}}
          style={{width: 100, height: 100}}
        />
        <Text style={{color: '#0e0e0e'}}>Binatang</Text>
      </View>
    );
  }
}
export default SampleComponent;
