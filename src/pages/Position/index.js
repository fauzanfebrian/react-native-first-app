import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Cart from '../../components/Cart';

const Position = () => {
  return (
    <View style={styles.container}>
      <Text>Ini materi position</Text>
      <Cart />
    </View>
  );
};

export default Position;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    alignItems: 'center',
  },
});
