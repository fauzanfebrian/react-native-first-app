import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import macbook from '../../assets/images/maxresdefault.jpg';

const Product = props => {
  return (
    <View style={styles.container}>
      <Image source={macbook} style={styles.fotoProduct} />
      <Text style={styles.nameProduct}>New Macbook Pro 2019</Text>
      <Text style={styles.price}>Rp. 25.000.000</Text>
      <Text style={styles.city}>Jakarta Barat</Text>
      <TouchableOpacity onPress={props.onButtonPress}>
        <View style={styles.buttonWrapper}>
          <Text style={styles.buttonText}>Beli</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Product;

const styles = StyleSheet.create({
  container: {
    padding: 12,
    backgroundColor: '#f2f2f2',
    width: 212,
    borderRadius: 8,
  },
  fotoProduct: {width: 188, height: 107, borderRadius: 8},
  nameProduct: {fontWeight: 'bold', fontSize: 14, marginTop: 16},
  city: {
    fontWeight: '300',
    fontSize: 12,
    marginTop: 14,
  },
  buttonWrapper: {
    backgroundColor: '#6fc697',
    paddingVertical: 6,
    borderRadius: 25,
    marginTop: 14,
  },
  buttonText: {
    fontWeight: '600',
    fontSize: 12,
    color: 'white',
    textAlign: 'center',
  },
  price: {
    fontWeight: 'bold',
    fontSize: 12,
    marginTop: 14,
    color: '#f2994a',
  },
});
