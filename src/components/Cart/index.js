import React from 'react';
import {StyleSheet, Text, View, Image, TouchableHighlight} from 'react-native';
import order from '../../assets/icons/order.png';

const Cart = props => {
  return (
    <>
      <View style={styles.cartWrapper}>
        <Image source={order} style={styles.iconCart} />
        <Text style={styles.notif}>{props.quantity}</Text>
        {props.quantity > 0 && (
          <TouchableHighlight
            onPress={props.reset}
            style={styles.buttonWrapper}>
            <Text style={styles.buttonText}>Reset</Text>
          </TouchableHighlight>
        )}
      </View>
      <Text style={styles.text}>Keranjang Belanjaan Anda</Text>
    </>
  );
};

export default Cart;

const styles = StyleSheet.create({
  cartWrapper: {
    borderColor: '#4398d1',
    borderWidth: 1,
    width: 93,
    height: 93,
    marginLeft: 15,
    borderRadius: 93 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  iconCart: {
    width: 50,
    height: 50,
  },
  text: {
    fontSize: 12,
    color: '#777777',
    fontWeight: '700',
    marginTop: 8,
  },
  notif: {
    textAlign: 'center',
    fontSize: 12,
    color: 'white',
    backgroundColor: '#6fc697',
    padding: 4,
    borderRadius: 25,
    width: 24,
    height: 24,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  buttonWrapper: {
    backgroundColor: 'red',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 8,
    position: 'absolute',
    bottom: -8,
  },
  buttonText: {
    fontWeight: '600',
    fontSize: 12,
    color: 'white',
    textAlign: 'center',
  },
});
